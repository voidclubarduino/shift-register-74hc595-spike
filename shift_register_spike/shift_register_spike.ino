#define LATCH_PIN 9 
#define CLOCK_PIN  10
#define DATA_PIN 8


void setup() {
  Serial.begin(9600);
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
}

void loop() { 

  if(Serial.available()){
     //ground latchPin and hold low for as long as you are transmitting
    digitalWrite(LATCH_PIN, LOW);
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, Serial.parseInt());   
    //return the latch pin high to signal chip that it 
    //no longer needs to listen for information
    digitalWrite(LATCH_PIN, HIGH);
  }
} 
 
